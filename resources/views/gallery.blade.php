<!DOCTYPE html>
<html lang="en">
    @include('layouts.head')
    <body class="fix-header fix-sidebar">
        <!-- Main wrapper  -->  
        <div id="main-wrapper">
            @include('layouts.header')
            @include('layouts.sidebar')
            <!-- Page wrapper  -->
            <div class="page-wrapper">
                <!-- Container fluid  -->
                <div class="container-fluid">
                    <!-- Start Page Content -->
                    <div class="container">

                        <div class="page-header">
                            <h1><i class="fa fa-image" aria-hidden="true"></i> Generate Meme</h1>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-title">
                                        <h4>Recent Orders </h4>
                                    </div>
                                    <div class="card-body">
                                   
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Image</th>
                                                        <th>Name</th>
                                                        <th>Format</th>
                                                        <th>Resalution</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                            <?php
                                                            $manuals= [];
                                                            $filesInFolder = \File::files('images');
                                                            foreach($filesInFolder as $path)
                                                            { 
                                                                ?>
                                                                 <tr>
                                                                <td>
                                                                <div class="round-img">
                                                                <img src="images/<?php echo pathinfo($path)['basename']; ?>"> 
                                                                </div>
                                                                </td>
                                                                <td><?php echo pathinfo($path,PATHINFO_FILENAME);?></td>
                                                                <td><?php echo pathinfo($path,PATHINFO_EXTENSION);?></td>
                                                                <td><?php list($width,$height)=getimagesize($path); echo $width." x ".$height?></td>\
                                                                </tr>
                                                             <?php
                                                             }
                                                             ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <!-- End PAge Content -->
                </div>
                <!-- End Container fluid  -->

                @include('layouts.footer')