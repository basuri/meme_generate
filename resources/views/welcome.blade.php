<!DOCTYPE html>
<html lang="en">
    @include('layouts.head')
    <body class="fix-header fix-sidebar">
        <!-- Main wrapper  -->  
        <div id="main-wrapper">
            @include('layouts.header')
            @include('layouts.sidebar')
            <!-- Page wrapper  -->
            <div class="page-wrapper">
                <!-- Container fluid  -->
                <div class="container-fluid">
                    <!-- Start Page Content -->
                    <div class="container">

                        <div class="page-header">
                            <h1><i class="fa fa-image" aria-hidden="true"></i> Generate Meme</h1>
                        </div>


                        <div class="row">
                            <div class="col-md-4">

                                <!-- BEGIN: Canvas rendering of generated Meme -->
                                <canvas id="meme" class="img-thumbnail img-fluid">
                                    Unfortunately your browser does not support canvas.
                                </canvas>
                                <!-- END: Canvas rendering of generated Meme -->
                                <a href="javascript:;" class="btn btn-primary btn-block" id="file_name">
                                <i class="fa fa-download" aria-hidden="true"></i> Download
                                   
                                    <input type="file" id="file" style="display: none"/>
                                    <input type="hidden" id="file_name"/>
                                    
                                </a>

                                <img id="start-image" src="image.jpg" alt="" />
                            </div>
                            <div class="col-md-8">

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="input-group">
                                            <!--span class="input-group-addon">Image:</span>
                                            <input type="file" class="form-control" id="imgInp" /-->
                                            <input type="file" class="custom-file-input" id="imgInp" required>
                                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                        </div>

                                    </div>
                                </div>



                                <hr />


                                <div class="form-group">
                                    <!--input type="text" class="form-control form-control-lg" value="Have you expected this to be" id="text_top" /-->
                                    <input type="text" id="text_top" class="form-control input-default " placeholder="Text One" stylr="border-radiour:4px;">
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3" for="text_top_offset">Offset from top:</label>
                                        <div class="col-md-7">
                                            <input style="width:100%" id="text_top_offset" type="range" min="0" max="500" value="50"/>
                                        </div>
                                        <div class="col-md-2 setting-value">
                                            <span id="text_top_offset__val">50</span>px
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <!--input type="text" class="form-control form-control-lg" value="funny?" id="text_bottom" /-->
                                    <input type="text" id="text_bottom" class="form-control input-default " placeholder="Text Two" stylr="border-radiour:4px;">
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3" for="text_bottom_offset">Offset from top:</label>
                                        <div class="col-md-7">
                                            <input style="width:100%" id="text_bottom_offset" type="range" min="0" max="500" value="450"/>
                                        </div>
                                        <div class="col-md-2 setting-value">
                                            <span id="text_bottom_offset__val">450</span>px
                                        </div>
                                    </div>
                                </div>

                                <hr/>

                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-md-3" for="canvas_size">Meme size:</label>
                                                        <div class="col-md-7">
                                                            <input style="width:100%" id="canvas_size" type="range" min="0" max="1000" value="500"/>
                                                        </div>
                                                        <div class="col-md-2 setting-value">
                                                            <span id="canvas_size__val">500</span>px
                                                        </div>
                                                    </div>
                                                </div>
                                


                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <i class="fa fa-cogs" aria-hidden="true"></i> More options
                                    </div>
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-md-3" for="text_font_size">Font size:</label>
                                                        <div class="col-md-7">
                                                            <input style="width:100%" id="text_font_size" type="range" min="0" max="72" value="28"/>
                                                        </div>
                                                        <div class="col-md-2 setting-value">
                                                            <span id="text_font_size__val">28</span>pt
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-md-3" for="text_line_height">Line height:</label>
                                                        <div class="col-md-7">
                                                            <input style="width:100%" id="text_line_height" type="range" min="0" max="100" value="15"/>
                                                        </div>
                                                        <div class="col-md-2 setting-value">
                                                            <span id="text_line_height__val">15</span>pt
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- End PAge Content -->
                </div>
                <!-- End Container fluid  -->
                @include('layouts.footer')